package com.sopra.digital.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sopra.digital.model.UserDeatils;
import com.sopra.digital.model.UserRepo;

@RestController
public class Controller {
	
	@Autowired
	private UserRepo repo;
	
	@Autowired
	private PasswordEncoder encoder;

	@GetMapping("/user")
	public Principal user(Principal user) {
		return user;
	}

	@PostMapping(value = "/create")
	public UserDeatils get(@RequestBody UserDeatils user) {
		user.setPassword(encoder.encode(user.getPassword()));
		return repo.save(user);
	}
}
