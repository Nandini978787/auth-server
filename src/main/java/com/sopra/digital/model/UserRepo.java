package com.sopra.digital.model;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface UserRepo extends CrudRepository<UserDeatils, String> {

	UserDeatils findByName(String name);
	
}
