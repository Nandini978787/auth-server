
package com.sopra.digital.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.session.SessionManagementFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private CorsFilter filter;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().and().addFilterBefore(filter, SessionManagementFilter.class).csrf().disable()
				.authorizeRequests().antMatchers("/oauth/token").permitAll().antMatchers("/create").permitAll();
	}

	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	/*
	 * @Bean public CorsFilter corsFilter() { CorsConfiguration config = new
	 * CorsConfiguration(); config.setAllowedOrigins(Arrays.asList("*"));
	 * config.setAllowedMethods(Arrays.asList("GET", "POST", "DELETE", "OPTIONS",
	 * "HEAD")); config.setAllowedHeaders(Arrays.asList("Content-Type",
	 * "content-type", "Access-Control-Allow-Origin", "authorization",
	 * "Authorization")); config.setAllowCredentials(true);
	 * UrlBasedCorsConfigurationSource source = new
	 * UrlBasedCorsConfigurationSource(); source.registerCorsConfiguration("/**",
	 * config); return new CorsFilter(source); }
	 */
}
