
package com.sopra.digital.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.web.session.SessionManagementFilter;
import org.springframework.web.filter.CorsFilter;

@Configuration
@EnableResourceServer
class ResourceServerConfig extends ResourceServerConfigurerAdapter {
	
	@Autowired
	private CorsFilter filter;

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.cors().and().addFilterBefore(filter, SessionManagementFilter.class).csrf().disable().authorizeRequests().antMatchers("/create").permitAll().anyRequest().authenticated();
	}
	
}
