FROM openjdk:8-jre-alpine
ADD target/auth-server-0.0.1-SNAPSHOT.jar   auth-server.jar
EXPOSE 5000
ENTRYPOINT ["java","-jar","auth-server.jar"]